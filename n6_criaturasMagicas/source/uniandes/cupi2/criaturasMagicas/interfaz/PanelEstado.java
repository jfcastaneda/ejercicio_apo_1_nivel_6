package uniandes.cupi2.criaturasMagicas.interfaz;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

/**
 * Ventana de la info del estado del usuario
 */
public class PanelEstado extends JPanel {

    /**
     * Atributo serial
     */
    private static final long serialVersionUID = 1L;

    /**
     * Label del puntaje
     */
    private JLabel puntaje;

    /**
     * Label del puntaje actual
     */
    private JLabel pActual;

    /**
     * Label de movimientos
     */
    private JLabel movimientos;

    /**
     * Label del movimiento actual
     */
    private JLabel mActual;

    /**
     * Conexion con la interfaz prinicpal
     */
    private InterfazCriaturasMagicas interfaz;

    /**
     * Constructor de la ventana de estado. <br>
     * <b>post: </b> Crea el panel.
     */
    public PanelEstado(InterfazCriaturasMagicas nInterfaz) {
        interfaz = nInterfaz;
        setForeground(Color.WHITE);
        setLayout(new GridLayout(1, 4));
        TitledBorder titledBorder = new TitledBorder("Estado del Juego");
        titledBorder.setTitleColor(Color.WHITE);
        setBorder(titledBorder);
        Border border = LineBorder.createGrayLineBorder();

        puntaje = new JLabel("Puntaje:");
        puntaje.setOpaque(false);
        puntaje.setForeground(Color.WHITE);
        add(puntaje);

        pActual = new JLabel();
        pActual.setForeground(Color.WHITE);
        pActual.setBorder(border);
        pActual.setMinimumSize(new Dimension(200, 20));
        pActual.setPreferredSize(new Dimension(200, 20));
        pActual.setMaximumSize(new Dimension(200, 20));
        pActual.setOpaque(false);
        add(pActual);

        movimientos = new JLabel("  Movimientos Restantes:");
        movimientos.setOpaque(false);
        movimientos.setForeground(Color.WHITE);
        add(movimientos);

        mActual = new JLabel();
        mActual.setForeground(Color.WHITE);
        mActual.setBorder(border);
        mActual.setMinimumSize(new Dimension(200, 20));
        mActual.setPreferredSize(new Dimension(200, 20));
        mActual.setMaximumSize(new Dimension(200, 20));
        mActual.setOpaque(false);
        add(mActual);

        setOpaque(false);
    }

    /**
     * Da la interfaz principal de la app
     *
     * @return La interfaz principal
     */
    public InterfazCriaturasMagicas darInterfaz() {
        return interfaz;
    }

    /**
     * Cambia el estado del usuario con la valores dadosp or parametro
     *
     * @param nPuntaje     El puntaje del usuario
     * @param nMovimientos Los movimientos del usuario
     */
    public void cambiarEstado(int nPuntaje, int nMovimientos) {
        pActual.setText(Integer.toString(nPuntaje));
        mActual.setText(Integer.toString(nMovimientos));
        repaint();
        revalidate();
    }
}
