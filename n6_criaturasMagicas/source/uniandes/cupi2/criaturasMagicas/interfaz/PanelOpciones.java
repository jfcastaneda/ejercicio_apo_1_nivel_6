package uniandes.cupi2.criaturasMagicas.interfaz;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 * Ventana de la opciones del usuario
 */
public class PanelOpciones extends JPanel implements ActionListener {

    /**
     * Constante serial
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constante de cargar
     */
    public static final String CARGAR = "cargar";

    /**
     * Constante de reiniciar
     */
    public static final String REINICIAR = "reiniciar";

    /**
     * Constante de opcion 1
     */
    public static final String OPCION_1 = "opcion 1";

    /**
     * Constante de opcion 2
     */
    public static final String OPCION_2 = "opcion 2";

    /**
     * Boton de cargar
     */
    private JButton cargar;

    /**
     * Boton de reiniciar
     */
    private JButton reiniciar;

    /**
     * Boton de opcion 1
     */
    private JButton opcion1;

    /**
     * Boton de opcion 2
     */
    private JButton opcion2;

    /**
     * Conexion con la interfaz principal
     */
    private InterfazCriaturasMagicas interfaz;

    /**
     * Constructor de la ventana de opciones. <br>
     * <b>post: </b> Crea el panel.
     */
    public PanelOpciones(InterfazCriaturasMagicas nInterfaz) {
        interfaz = nInterfaz;
        setForeground(Color.WHITE);
        setOpaque(false);
        setLayout(new GridLayout(1, 4));
        TitledBorder titledBorder = new TitledBorder("Opciones");
        titledBorder.setTitleColor(Color.WHITE);
        setBorder(titledBorder);

        cargar = new JButton("Cargar");
        cargar.setHorizontalAlignment(JLabel.CENTER);
        cargar.setActionCommand(CARGAR);
        cargar.addActionListener(this);
        add(cargar);

        reiniciar = new JButton("Reiniciar");
        reiniciar.setHorizontalAlignment(JLabel.CENTER);
        reiniciar.setActionCommand(REINICIAR);
        reiniciar.addActionListener(this);
        reiniciar.setEnabled(false);
        add(reiniciar);

        opcion1 = new JButton("Opcion 1");
        opcion1.setHorizontalAlignment(JLabel.CENTER);
        opcion1.setActionCommand(OPCION_1);
        opcion1.addActionListener(this);
        add(opcion1);

        opcion2 = new JButton("Opcion 2");
        opcion2.setHorizontalAlignment(JLabel.CENTER);
        opcion2.setActionCommand(OPCION_2);
        opcion2.addActionListener(this);
        add(opcion2);

    }

    /**
     * Da la interfaz principal de la app
     *
     * @return La interfaz principal
     */
    public InterfazCriaturasMagicas darInterfaz() {
        return interfaz;
    }

    /**
     * Acciones de los botones
     */
    public void actionPerformed(ActionEvent e) {
        String comd = e.getActionCommand();
        if (comd.equals(CARGAR)) {
            interfaz.cargar();
        } else if (comd.equals(REINICIAR)) {
            interfaz.reiniciar();
        } else if (comd.equals(OPCION_1)) {
            interfaz.opcion1();
        } else if (comd.equals(OPCION_2)) {
            interfaz.opcion2();
        }
    }

    /**
     * Activa el boton reiniciar.
     */
    public void activarReiniciar()
    {
        reiniciar.setEnabled(true);
    }
}
