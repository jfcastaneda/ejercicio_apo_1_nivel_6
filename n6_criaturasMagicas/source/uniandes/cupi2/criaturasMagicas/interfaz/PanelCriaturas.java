package uniandes.cupi2.criaturasMagicas.interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import uniandes.cupi2.criaturasMagicas.mundo.Criatura;

/**
 * Ventana de la info de las criaturas
 */
public class PanelCriaturas extends JPanel implements ActionListener {
    /**
     * Atributo serial
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constante para izquierda
     */
    public static final String IZQUIERDA = "izquierda";

    /**
     * Constante para derecha
     */
    public static final String DERECHA = "derecha";

    /**
     * Label de titulo
     */
    private JLabel titulo;

    /**
     * Label de puntaje
     */
    private JLabel puntaje;

    /**
     * Label de puntaje
     */
    private JLabel imagen;

    /**
     * Label de gustos
     */
    private JLabel gustos;

    /**
     * Label de gustos actual
     */
    private JLabel gActual;

    /**
     * Label de miedos
     */
    private JLabel miedos;

    /**
     * Label de miedos actual
     */
    private JLabel mActual;

    /**
     * Checkbox de ser de luz
     */
    private JCheckBox serLuz;

    /**
     * Label de ser de luz
     */
    private JLabel lActual;

    /**
     * Boton de derecha
     */
    private JButton derecha;

    /**
     * Boton de izquierda
     */
    private JButton izquierda;

    /**
     * Conexion con interfaz principal
     */
    private InterfazCriaturasMagicas interfaz;

    /**
     * Constructor de la ventana de criaturas. <br>
     * <b>post: </b> Crea el panel.
     */
    public PanelCriaturas(InterfazCriaturasMagicas nInterfaz) {
        interfaz = nInterfaz;
        setForeground(Color.WHITE);
        setLayout(new GridLayout(2, 1));
        TitledBorder titledBorder = new TitledBorder("Enciclopedia");
        titledBorder.setTitleColor(Color.WHITE);
        setBorder(titledBorder);
        Border border = LineBorder.createGrayLineBorder();

        JPanel panelArriba = new JPanel();
        panelArriba.setOpaque(false);
        panelArriba.setLayout(new BorderLayout());

        JPanel panelAux = new JPanel();
        panelAux.setOpaque(false);
        panelAux.setLayout(new GridLayout(2, 1));


        titulo = new JLabel();
        titulo.setOpaque(false);
        titulo.setForeground(Color.WHITE);
        titulo.setFont(new Font("Helvetica", Font.BOLD, 18));
        titulo.setHorizontalAlignment(JLabel.CENTER);
        panelAux.add(titulo);

        puntaje = new JLabel();
        puntaje.setOpaque(false);
        puntaje.setForeground(Color.WHITE);
        puntaje.setFont(new Font("Helvetica", Font.BOLD, 16));
        puntaje.setHorizontalAlignment(JLabel.CENTER);
        panelAux.add(puntaje);
        panelArriba.add(panelAux, BorderLayout.NORTH);

        imagen = new JLabel();
        imagen.setOpaque(false);
        imagen.setMinimumSize(new Dimension(200, 200));
        imagen.setPreferredSize(new Dimension(200, 200));
        imagen.setMaximumSize(new Dimension(200, 200));
        imagen.setHorizontalAlignment(JLabel.CENTER);
        panelArriba.add(imagen, BorderLayout.CENTER);


        add(panelArriba);

        JPanel panelAbajo = new JPanel();
        panelAbajo.setOpaque(false);
        panelAbajo.setLayout(new GridLayout(7, 1));

        JPanel panelSup = new JPanel();
        panelSup.setOpaque(false);
        panelAbajo.add(panelSup);

        gustos = new JLabel("Gustos:");
        gustos.setOpaque(false);
        gustos.setForeground(Color.WHITE);
        panelAbajo.add(gustos);

        gActual = new JLabel();
        gActual.setForeground(Color.WHITE);
        gActual.setBorder(border);
        gActual.setMinimumSize(new Dimension(200, 10));
        gActual.setPreferredSize(new Dimension(200, 10));
        gActual.setMaximumSize(new Dimension(200, 10));
        gActual.setOpaque(false);
        panelAbajo.add(gActual);

        miedos = new JLabel("Miedos:");
        miedos.setOpaque(false);
        miedos.setForeground(Color.WHITE);
        panelAbajo.add(miedos);

        mActual = new JLabel();
        mActual.setForeground(Color.WHITE);
        mActual.setBorder(border);
        mActual.setMinimumSize(new Dimension(200, 10));
        mActual.setPreferredSize(new Dimension(200, 10));
        mActual.setMaximumSize(new Dimension(200, 10));
        mActual.setOpaque(false);
        panelAbajo.add(mActual);

        JPanel panelMedio1 = new JPanel();
        panelMedio1.setOpaque(false);
        panelMedio1.setLayout(new BorderLayout());

        serLuz = new JCheckBox();
        serLuz.setOpaque(false);
        serLuz.setForeground(Color.WHITE);
        serLuz.setEnabled(false);
        panelMedio1.add(serLuz, BorderLayout.WEST);

        lActual = new JLabel(" Ser de Luz");
        lActual.setOpaque(false);
        lActual.setForeground(Color.GRAY);
        panelMedio1.add(lActual, BorderLayout.CENTER);

        panelAbajo.add(panelMedio1);

        JPanel panelMedio2 = new JPanel();
        panelMedio2.setOpaque(false);
        panelMedio2.setLayout(new GridLayout(1, 2));

        derecha = new JButton(">");
        derecha.setActionCommand(DERECHA);
        derecha.addActionListener(this);

        izquierda = new JButton("<");
        izquierda.setActionCommand(IZQUIERDA);
        izquierda.addActionListener(this);

        panelMedio2.add(izquierda);
        panelMedio2.add(derecha);

        panelAbajo.add(panelMedio2);

        add(panelAbajo);

        setBackground(new Color(48, 41, 84));
    }

    /**
     * Da la interfaz principal de la app
     *
     * @return La interfaz principal
     */
    public InterfazCriaturasMagicas darInterfaz() {
        return interfaz;
    }

    /**
     * Actualiza el panel con la criatura dada por parametro
     *
     * @param actual La criatura con la info a mostrar
     */
    public void actualizarCriatura(Criatura actual) {
        titulo.setText(actual.darNombre());
        puntaje.setText(Integer.toString(actual.darPuntos()));
        ImageIcon icon = new ImageIcon(new ImageIcon(actual.darRutaImagen()).getImage().getScaledInstance(200, 200, Image.SCALE_DEFAULT));
        imagen.setIcon(icon);
        gActual.setText(actual.darGustos());
        mActual.setText(actual.darMiedos());
        if (actual.esSerDeLuz()) {
            serLuz.setSelected(true);
        } else {
            serLuz.setSelected(false);
        }
        repaint();
        revalidate();
    }

    /**
     * Acciones de los botones
     */
    public void actionPerformed(ActionEvent e) {
        String comd = e.getActionCommand();
        if (comd.equals(DERECHA)) {
            interfaz.darSiguiente();
        } else if (comd.equals(IZQUIERDA)) {
            interfaz.darAnterior();
        }
    }
}
