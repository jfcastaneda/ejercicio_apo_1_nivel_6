package uniandes.cupi2.criaturasMagicas.interfaz;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Ventana de la imagen superior
 */
public class PanelImagen extends JPanel {

    /**
     * Constante serial
     */
    private static final long serialVersionUID = 1L;

    /**
     * Label de la imagen
     */
    private JLabel imagen;

    /**
     * Constructor de la ventana de imagen. <br>
     * <b>post: </b> Crea el panel.
     */
    public PanelImagen() {
        imagen = new JLabel();
        ImageIcon icon = new ImageIcon("./data/imagenes/banner.png");
        imagen.setIcon(icon);
        add(imagen);
        setOpaque(false);
    }

}