package uniandes.cupi2.criaturasMagicas.interfaz;

import uniandes.cupi2.criaturasMagicas.mundo.Casilla;
import uniandes.cupi2.criaturasMagicas.mundo.CriaturasMagicas;

import java.awt.*;
import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PanelMapa extends JPanel {

    /**
     * Interfaz.
     */
    private InterfazCriaturasMagicas interfaz;

    /**
     * Matriz de Labls que representa un tablero.
     */
    private JLabel[][] tableroPanel;

    /**
     * Constructor.
     *
     * @param interfaz
     */
    public PanelMapa(InterfazCriaturasMagicas interfaz) {
        this.interfaz = interfaz;
        setOpaque(false);
    }

    public void cargar(Casilla[][] tablero) {
        removeAll();
        setLayout(new GridLayout(tablero.length, tablero[0].length));
        tableroPanel = new JLabel[tablero.length][tablero[0].length];
        ImageIcon icon;

        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero[0].length; j++) {
                icon = new ImageIcon(tablero[i][j].getTipoCasilla().getRuta());
                tableroPanel[i][j] = new JLabel();
                tableroPanel[i][j].addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        super.mouseClicked(e);
                        mouseClicked2((JLabel) e.getSource());
                    }
                });
                add(tableroPanel[i][j]);
            }
        }
        actualizar(tablero);
    }

    /**
     * Actualiza como se ve la matriz.
     */
    public void actualizar(Casilla[][] tablero) {
        for (int i = 0; i < tableroPanel.length; i++) {
            for (int j = 0; j < tableroPanel[i].length; j++) {
                if (tablero[i][j].getTipoCasilla().equals(Casilla.TipoCasilla.Tipo4)
                        && tablero[i][j].getCriatura() != null) {
                    ImageIcon imageIcon = new ImageIcon(tablero[i][j].getCriatura().darRutaImagen());
                    Image image = imageIcon.getImage();
                    image = image.getScaledInstance(tableroPanel[i][j].getWidth(), tableroPanel[i][j].getHeight(),
                            Image.SCALE_DEFAULT);
                    imageIcon.setImage(image);
                    tableroPanel[i][j].setIcon(imageIcon);
                    if (!tablero[i][j].isActualizado()) {
                        ImageIcon imageIcon1 = new ImageIcon(tablero[i][j].getCriatura().darRutaImagen());
                        image = imageIcon1.getImage();
                        image = image.getScaledInstance(170, 170, Image.SCALE_DEFAULT);
                        imageIcon1 = new ImageIcon(image);
                        tablero[i][j].actualizar();
                        JOptionPane.showMessageDialog(interfaz,
                                "Encontraste 1 " + tablero[i][j].getCriatura().darNombre(),
                                "Visitar Casilla",
                                JOptionPane.INFORMATION_MESSAGE,
                                imageIcon1);
                    }

                } else {
                    tableroPanel[i][j].setIcon(new ImageIcon(tablero[i][j].getTipoCasilla().getRuta()));
                }
            }
        }
        validate();
        repaint();
    }

    /**
     * Lo que pasa cuando al mouse le dan clic.
     *
     * @param jLabel
     */
    public void mouseClicked2(JLabel jLabel) {
        interfaz.mouseClicked(jLabel);
    }

    public void click(JLabel jLabel, Casilla[][] tablero) {
        boolean encontrado = false;
        int fila = 0;
        int columna = 0;

        for (int i = 0; i < tableroPanel.length && !encontrado; i++) {
            for (int j = 0; j < tableroPanel[i].length && !encontrado; j++) {
                if (jLabel.equals(tableroPanel[i][j])) {
                    fila = i;
                    columna = j;
                    encontrado = true;
                }
            }
        }
        try {
            interfaz.click(fila, columna);
            actualizar(tablero);
        } catch (Exception e) {
            actualizar(tablero);
            String mensaje = e.getMessage();
            if (mensaje.equals(CriaturasMagicas.MensajesClic.Ganador.getMensaje())) {
                JOptionPane.showMessageDialog(interfaz, e.getMessage(), "Visitar Casilla",
                        JOptionPane.INFORMATION_MESSAGE);
            } else if (mensaje.equals(CriaturasMagicas.MensajesClic.PerdedorPuntaje.getMensaje())
                    || mensaje.equals(CriaturasMagicas.MensajesClic.PerdedorMovimientos.getMensaje())) {
                JOptionPane.showMessageDialog(interfaz, e.getMessage(), "Visitar Casilla",
                        JOptionPane.WARNING_MESSAGE);
            }
            removeAll();
            interfaz.desactivarBotones();
        }
    }
}
