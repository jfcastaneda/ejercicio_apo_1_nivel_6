package uniandes.cupi2.criaturasMagicas.interfaz;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 * Ventana de las acciones a realizar por el usuario
 */
public class PanelAcciones extends JPanel implements ActionListener {

    /**
     * Atributo serial
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constante para criatura por fila
     */
    public static final String CRIATURA_POR_FILA = "criatura por fila";

    /**
     * Constante para criatura por columna
     */
    public static final String CRIATURA_POR_COLUMNA = "criatura por columna";

    /**
     * Constante para puntaje por cuadrante
     */
    public static final String PUNTAJE_POR_CUADRANTE = "puntaje por cuadrante";

    /**
     * Constante para criatura con mayor puntaje
     */
    public static final String CRIATURA_MAYOR_PUNTAJE = "criatura mayor puntaje";

    /**
     * Boton de criatura por fila
     */
    private JButton cFila;

    /**
     * Boton de criatura por columna
     */
    private JButton cColumna;

    /**
     * Boton de puntaje por cuadrante
     */
    private JButton pCuadrante;

    /**
     * Boton de criatura con mayor puntaje
     */
    private JButton cMayor;

    /**
     * Conexion con la interfaz principal
     */
    private InterfazCriaturasMagicas interfaz;

    /**
     * Constructor de la ventana de acciones. <br>
     * <b>post: </b> Crea el panel.
     */
    public PanelAcciones(InterfazCriaturasMagicas nInterfaz) {
        interfaz = nInterfaz;
        setForeground(Color.WHITE);
        setOpaque(false);
        setLayout(new GridLayout(1, 4));
        TitledBorder titledBorder = new TitledBorder("Acciones");
        titledBorder.setTitleColor(Color.WHITE);
        setBorder(titledBorder);

        cFila = new JButton("Criaturas por Fila");
        cFila.setEnabled(false);
        cFila.setActionCommand(CRIATURA_POR_FILA);
        cFila.addActionListener(this);
        cFila.setHorizontalAlignment(JLabel.CENTER);
        add(cFila);

        cColumna = new JButton("Criaturas por Columna");
        cColumna.setEnabled(false);
        cColumna.setActionCommand(CRIATURA_POR_COLUMNA);
        cColumna.addActionListener(this);
        cColumna.setHorizontalAlignment(JLabel.CENTER);
        add(cColumna);

        pCuadrante = new JButton("Puntaje por Cuadrante");
        pCuadrante.setEnabled(false);
        pCuadrante.setActionCommand(PUNTAJE_POR_CUADRANTE);
        pCuadrante.addActionListener(this);
        pCuadrante.setHorizontalAlignment(JLabel.CENTER);
        add(pCuadrante);

        cMayor = new JButton("Criatura Mayor Puntaje");
        cMayor.setEnabled(false);
        cMayor.setActionCommand(CRIATURA_MAYOR_PUNTAJE);
        cMayor.addActionListener(this);
        cMayor.setHorizontalAlignment(JLabel.CENTER);
        add(cMayor);
    }

    /**
     * Habilita los botones deshabilitados
     */
    public void habilitarBotones() {
        cFila.setEnabled(true);
        cColumna.setEnabled(true);
        pCuadrante.setEnabled(true);
        cMayor.setEnabled(true);
    }

    /**
     * Desactiva los botones.
     */
    public void desactivarBotones()
    {
        cFila.setEnabled(false);
        cColumna.setEnabled(false);
        pCuadrante.setEnabled(false);
        cMayor.setEnabled(false);
    }

    /**
     * Da la interfaz principal de la app
     *
     * @return La interfaz principal
     */
    public InterfazCriaturasMagicas darInterfaz() {
        return interfaz;
    }

    /**
     * Acciones de los botones
     */
    public void actionPerformed(ActionEvent e) {
        String comd = e.getActionCommand();
        if (comd.equals(CRIATURA_POR_FILA)) {
            interfaz.darCriaturasPorFila();
        } else if (comd.equals(CRIATURA_POR_COLUMNA)) {
            interfaz.darCriaturasPorColumna();
        } else if (comd.equals(PUNTAJE_POR_CUADRANTE)) {
            interfaz.darCriaturasPorCuadrante();
        } else if (comd.equals(CRIATURA_MAYOR_PUNTAJE)) {
            interfaz.darCriaturaMayorPuntaje();
        }
    }
}
