package uniandes.cupi2.criaturasMagicas.interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.*;

import uniandes.cupi2.criaturasMagicas.mundo.Criatura;
import uniandes.cupi2.criaturasMagicas.mundo.CriaturasMagicas;

/**
 * Ventana principal de la App
 */
public class InterfazCriaturasMagicas extends JFrame {

    /**
     * Tipos de archivos validos que reconoce el metodo cargar.
     */
    private enum ArchivosValidos {
        mapa1("mapa1.properties"),
        mapa2("mapa2.properties"),
        mapa3("mapa3.properties");

        /**
         * Ruta del archivo.
         */
        private String ruta;

        /**
         * Constructor.
         *
         * @param ruta
         */
        ArchivosValidos(String ruta) {
            this.ruta = ruta;
        }

        /**
         * Metodo que indica si una ruta pertenece a las rutas validas.
         *
         * @param ruta
         * @return true si pertenece, false si no.
         */
        private static boolean esArchivoValido(String ruta) {
            return ruta.equals(mapa1.ruta) || ruta.equals(mapa2.ruta) || ruta.equals(mapa3.ruta);
        }
    }

    /**
     * Atributo serial
     */
    private static final long serialVersionUID = 1L;

    /**
     * Panel de imagen superior
     */
    private PanelImagen panelImagen;

    /**
     * Panel del estado de movimientos y puntaje del jugador
     */
    private PanelEstado panelEstado;

    /**
     * Panel de la informacion de las criaturas
     */
    private PanelCriaturas panelCriaturas;

    /**
     * Panel de las acciones posibles por el usuario
     */
    private PanelAcciones panelAcciones;

    /**
     * Panel de las opciones posibles por el usuario
     */
    private PanelOpciones panelOpciones;

    /**
     * Label de la iamgen del tablero
     */
    private PanelMapa panelMapa;

    /**
     * Clase principal del mundo
     */
    private CriaturasMagicas mundo;

    /**
     * Mundo para cuando se reinicie.
     */
    private File archivoReinicio;

    /**
     * Constructor de la ventana principal. <br>
     * <b>post: </b> Crea la interfaz y actualiza los paneles con la info.
     */
    public InterfazCriaturasMagicas() {
        try {
            mundo = new CriaturasMagicas();
        } catch (Exception e) {
            // Nunca pasa
        }

        setTitle("Criaturas Magicas");
        setSize(780, 750);
        setResizable(false);
        getContentPane().setBackground(new Color(48, 41, 84));
        setLayout(new BorderLayout());

        JPanel panelArriba = new JPanel();
        panelArriba.setOpaque(false);
        panelImagen = new PanelImagen();
        panelEstado = new PanelEstado(this);
        panelArriba.setLayout(new BorderLayout());
        panelArriba.add(panelImagen, BorderLayout.NORTH);
        panelArriba.add(panelEstado, BorderLayout.SOUTH);
        add(panelArriba, BorderLayout.NORTH);

        JPanel panelMitad = new JPanel();
        panelMitad.setLayout(new BorderLayout());

        panelMapa = new PanelMapa(this);

        panelCriaturas = new PanelCriaturas(this);
        panelMitad.add(panelMapa, BorderLayout.CENTER);
        panelMitad.add(panelCriaturas, BorderLayout.EAST);
        add(panelMitad, BorderLayout.CENTER);

        JPanel panelAbajo = new JPanel();
        panelAbajo.setOpaque(false);
        panelAbajo.setLayout(new GridLayout(2, 1));

        panelAcciones = new PanelAcciones(this);
        panelAbajo.add(panelAcciones);

        panelOpciones = new PanelOpciones(this);
        panelAbajo.add(panelOpciones);

        add(panelAbajo, BorderLayout.SOUTH);
        panelEstado.cambiarEstado(0, 0);
        actualizarCriaturas(mundo.darActual());
    }

    /**
     * Actualiza el panel de Estado con info nueva
     */
    public void actualizarPanelEstado() {
        int puntaje = mundo.darPuntaje();
        int movimientos = mundo.darMovimientosRestantes();
        panelEstado.cambiarEstado(puntaje, movimientos);
    }

    /**
     * Actualiza el panel de Criaturas con la criatura de parametro
     *
     * @param actual La criatura a mostrar
     */
    public void actualizarCriaturas(Criatura actual) {
        panelCriaturas.actualizarCriatura(actual);
    }

    /**
     * Da la siguiente criatura a la actual
     */
    public void darSiguiente() {
        try {
            actualizarCriaturas(mundo.darSiguiente());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

    /**
     * Da la anterior criatura a la actual
     */
    public void darAnterior() {
        try {
            actualizarCriaturas(mundo.darAnterior());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

    /**
     * Carga la imagen del mapa en el Label
     */
    public void cargar() {
        File archivoMapa = null;
        JFileChooser fc = new JFileChooser("./data");
        fc.setDialogTitle("Abrir archivo de campeonato");
        int resultado = fc.showOpenDialog(this);

        try {
            if (resultado == JFileChooser.APPROVE_OPTION) {
                archivoMapa = fc.getSelectedFile();
                if (ArchivosValidos.esArchivoValido(archivoMapa.getName())) {
                    panelAcciones.habilitarBotones();
                    mundo = new CriaturasMagicas(archivoMapa);
                    archivoReinicio = archivoMapa;
                    panelEstado.cambiarEstado(mundo.darPuntaje(), mundo.darMovimientosRestantes());
                    panelMapa.cargar(mundo.getTablero());
                } else {
                    JOptionPane.showMessageDialog(this, "Ese archivo no es valido",
                            "Cargar", JOptionPane.ERROR_MESSAGE);
                }
            }
            panelOpciones.activarReiniciar();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Cargar", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void mouseClicked(JLabel jLabel) {
        panelMapa.click(jLabel, mundo.getTablero());
    }

    /**
     * Metodo que maneja las acciones que dependen de un clic.
     *
     * @param fila
     * @param columna
     */
    public void click(int fila, int columna) throws Exception {
        mundo.click(fila, columna);
        actualizarPanelEstado();
    }

    /**
     * Metodo que desactiva los botones.
     */
    public void desactivarBotones() {
        panelAcciones.desactivarBotones();
    }

    /**
     * Reinicia la App, no hace nada
     */
    public void reiniciar() {
        try {
            panelAcciones.habilitarBotones();
            mundo = new CriaturasMagicas(archivoReinicio);
            panelEstado.cambiarEstado(mundo.darPuntaje(), mundo.darMovimientosRestantes());
            panelMapa.cargar(mundo.getTablero());

            panelOpciones.activarReiniciar();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Cargar", JOptionPane.ERROR_MESSAGE);
        }
        panelAcciones.habilitarBotones();
        panelEstado.cambiarEstado(mundo.darPuntaje(), mundo.darMovimientosRestantes());
        panelMapa.cargar(mundo.getTablero());
    }

    /**
     * Reservado para extension 1
     */
    public void opcion1() {
        String r = mundo.metodo1();
        JOptionPane.showMessageDialog(this, r, "Opci�n 2", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Reservado para extension 2
     */
    public void opcion2() {
        String r = mundo.metodo2();
        JOptionPane.showMessageDialog(this, r, "Opci�n 2", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Encargado de verificar si el texto ingresado es un numero o no
     *
     * @param texto El texto de ingreso del usuario
     * @return true si es unnumero o false si no lo es
     */
    private static boolean esNumero(String texto) {
        try {
            Integer.parseInt(texto);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    /**
     * Da el numero de criaturas por fila
     */
    public void darCriaturasPorFila() {
        String r = JOptionPane.showInputDialog(this, "Ingrese la Fila");
        if (esNumero(r)) {
            int numero = Integer.parseInt(r);
            if (numero < 1) {
                JOptionPane.showMessageDialog(this, "El numero ingresado no es valido", "Criaturas por Fila", JOptionPane.ERROR_MESSAGE);
            } else {
                String msg = "En la fila indicada hay " + mundo.darCantidadCriaturasPorFila(numero) + " criaturas.";
                JOptionPane.showMessageDialog(this, msg, "Criaturas por Fila", JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "El valor de la fila no es valido", "Criaturas por Fila", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Da el numero de criaturas por columna
     */
    public void darCriaturasPorColumna() {
        String r = JOptionPane.showInputDialog(this, "Ingrese la Columna");
        if (esNumero(r)) {
            int numero = Integer.parseInt(r);
            if (numero < 1) {
                JOptionPane.showMessageDialog(this, "El numero ingresado no es valido", "Criaturas por Columna", JOptionPane.ERROR_MESSAGE);
            } else {
                String msg = "En la columna indicada hay " + mundo.darCantidadCriaturasPorColumna(numero) + " criaturas.";
                JOptionPane.showMessageDialog(this, msg, "Criaturas por Columna", JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "El valor de la columna no es valido", "Criaturas por Columna", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Da el numero de criaturas por cuadrante
     */
    public void darCriaturasPorCuadrante() {
        String r = JOptionPane.showInputDialog(this, "Ingrese el Cuadrante");
        if (esNumero(r)) {
            int numero = Integer.parseInt(r);
            if (numero < 1) {
                JOptionPane.showMessageDialog(this, "El cuadrante especificado no es valido", "Criaturas por Cuadrante", JOptionPane.ERROR_MESSAGE);
            } else if (numero > 4) {
                JOptionPane.showMessageDialog(this, "El cuadrante especificado no es valido", "Criaturas por Cuadrante", JOptionPane.ERROR_MESSAGE);
            } else {
                String msg = "En el cuadrante indicado hay " + mundo.calcularPuntajePorCuadrante(numero) + " puntos por obtener.";
                JOptionPane.showMessageDialog(this, msg, "Criaturas por Cuadrante", JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "El valor del cuadrante no es valido", "Criaturas por Cuadrante", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Da la criatura con el mayor puntaje en la App
     */
    public void darCriaturaMayorPuntaje() {
        String r = "La criatura sin encontrar con mayor puntaje es " + mundo.darCriaturaMayorPuntaje().darNombre();
        JOptionPane.showMessageDialog(this, r, "Fila Mas visitada", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Ejecuta la aplicaci�n.
     *
     * @param args Par�metros de la ejecuci�n.
     */
    public static void main(String[] args) {

        CriaturasMagicas mundo;
        try {
            InterfazCriaturasMagicas interfaz = new InterfazCriaturasMagicas();
            interfaz.setVisible(true);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}
