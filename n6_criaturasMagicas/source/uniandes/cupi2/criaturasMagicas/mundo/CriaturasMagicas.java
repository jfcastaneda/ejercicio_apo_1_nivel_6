/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Universidad de los Andes (Bogot� - Colombia)
 * Departamento de Ingenier�a de Sistemas y Computaci�n
 * Licenciado bajo el esquema Academic Free License version 2.1
 * <p>
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
 * Ejercicio: n5_criaturasMagicas
 * Autor: Equipo Cupi2 2018
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package uniandes.cupi2.criaturasMagicas.mundo;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Juego de la b�squeda de criaturas m�gicas.
 */
public class CriaturasMagicas {

    /**
     * Enum de los tipos de mensajes que tienen las exceptions del clic.
     */
    public enum MensajesClic {
        Ganador("Su puntaje es mayor o igual a 5000, ha ganado el juego"),
        PerdedorPuntaje("Su puntaje es menor a -3000, ha perdido el juego"),
        PerdedorMovimientos("Se ha quedado sin movimientos, ha perdido el juego");

        /**
         * Texto del mensaje.
         */
        private String mensaje;

        /**
         * Constructor
         *
         * @param mensaje
         */
        MensajesClic(String mensaje) {
            this.mensaje = mensaje;
        }

        /**
         * Retorna el mensaje.
         */
        public String getMensaje() {
            return mensaje;
        }
    }

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Enciclopedia de criaturas que se pueden encontrar.
     */
    private Criatura[] enciclopedia;

    /**
     * Los �ltimos datos cargados de las criaturas.
     */
    private Properties datosCriaturas;

    /**
     * Los ultimos datos cargados del mapa.
     */
    private Properties datosMapa;

    /**
     * Criatura actual en la navegaci�n.
     */
    private int criaturaActual;

    /**
     * Cantidad de movimientos que le quedan al jugador.
     */
    private int cantidadMovimientos;

    /**
     * Cantidad de criaturas presentes en el tablero.
     */
    private int cantidadCriaturas;

    /**
     * Puntaje del jugador.
     */
    private int puntaje;

    /**
     * Matriz que representa el tablero de juego.
     */
    private Casilla[][] tablero;

    // -----------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------

    /**
     * Crea la instancia principal de la aplicaci�n y carga la enciclopedia de criaturas. <br>
     * <b>post: </b> Se ha cargado la informaci�n del arreglo de criaturas.
     *
     * @throws Exception Si hubo error al cargar el archivo.
     *                   Si hubo error al leer el formato del archivo.
     */
    public CriaturasMagicas() throws Exception {
        puntaje = 0;
        inicializarCriaturas();
    }

    /**
     * Crea la instancia principal de la aplicaci�n y carga la enciclopedia de criaturas. <br>
     * <b>post: </b> Se ha cargado la informaci�n del arreglo de criaturas.
     *
     * @param arch Archivo del que se carga la informacion.
     * @throws Exception Si hubo error al cargar el archivo.
     *                   Si hubo error al leer el formato del archivo.
     */
    public CriaturasMagicas(File arch) throws Exception {
        cargar(arch);
        inicializarCriaturas();
        inicializarTablero();
    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Retorna la criatura actual de la enciclopedia.
     *
     * @return La criatura actual de la enciclopedia.
     */
    public Criatura darActual() {
        return enciclopedia[criaturaActual];
    }

    /**
     * Retorna la siguiente criatura a la criatura actual. <br>
     * <b>post: </b> Se ha modificado la criatura actual a la siguiente criatura.
     *
     * @return La siguiente criatura.
     * @throws Exception Si ya se encuentra en la �ltima criatura de la enciclopedia.
     */
    public Criatura darSiguiente() throws Exception {
        if (criaturaActual == enciclopedia.length - 1) {
            throw new Exception("Ya se encuentra en la �ltima criatura.");
        }

        criaturaActual++;
        return enciclopedia[criaturaActual];
    }

    /**
     * Retorna la anterior criatura a la criatura actual. <br>
     * <b>post: </b> Se ha modificado la criatura actual a la anterior criatura.
     *
     * @return La anterior criatura.
     * @throws Exception Si ya se encuentra en la primera criatura de la enciclopedia.
     */
    public Criatura darAnterior() throws Exception {
        if (criaturaActual == 0) {
            throw new Exception("Ya se encuentra en la primera criatura.");
        }

        criaturaActual--;
        return enciclopedia[criaturaActual];
    }

    /**
     * Retorna el puntaje del jugador. Este valor siempre es 0.
     *
     * @return El  puntaje del jugador.
     */
    public int darPuntaje() {
        return puntaje;
    }

    /**
     * Retorna la cantidad de movimientos restantes del jugador. Este valor siempre es 10.
     *
     * @return La cantidad de movimientos restantes del jugador.
     */
    public int darMovimientosRestantes() {
        return cantidadMovimientos;
    }

    /**
     * Busca una criatura por su nombre en la enciclopedia de criaturas.
     *
     * @param pNombre Nombre de la criatura. pNombre != null && pNombre != "".
     * @return La criatura con el nombre especificado. Si no la encuentra retorna null.
     */
    public Criatura buscarCriatura(String pNombre) {
        Criatura buscada = null;
        for (int i = 0; i < enciclopedia.length && buscada == null; i++) {
            if (enciclopedia[i].darNombre().equals(pNombre)) {
                buscada = enciclopedia[i];
            }
        }
        return buscada;
    }

    /**
     * Metodo que gestiona las acciones cuando se da clic a una casilla.
     * @param fila
     * @param columna
     * @throws Exception dependiendo del evento que pasa
     */
    public void click(int fila, int columna) throws Exception {
        puntaje += tablero[fila][columna].click();
        cantidadMovimientos--;
        if (puntaje >= 5000) {
            throw new Exception(MensajesClic.Ganador.getMensaje());
        } else if (puntaje < -3000) {
            throw new Exception(MensajesClic.PerdedorPuntaje.getMensaje());
        } else if (cantidadMovimientos == 0) {
            throw new Exception(MensajesClic.PerdedorMovimientos.getMensaje());
        }
    }

    /**
     * Carga el archivo especificado por par�metro para procesarlo.
     *
     * @param arch Archivo del que se carga la informacion.
     * @throws Exception Si se encuentra alg�n problema al cargar el archivo.
     */
    private void cargar(File arch) throws Exception {
        puntaje = 0;
        datosMapa = new Properties();
        FileInputStream in = new FileInputStream(arch);
        try {
            datosMapa.load(in);
            in.close();

        } catch (IOException e) {
            throw new Exception("Error al cargar el archivo, archivo no v�lido.");
        }
    }

    /**
     * Inicializa el arreglo de criaturas a partir del archivo de configuraci�n. <br>
     * <b>post: </b> Ha sido inicializada la enciclopedia de criaturas.
     *
     * @throws Exception Lanza excepci�n si hay alg�n error al inicializar las criaturas.
     */
    private void inicializarCriaturas() throws Exception {
        try {
            datosCriaturas = new Properties();
            FileInputStream in = new FileInputStream("./data/criaturas.properties");
            try {
                datosCriaturas.load(in);
                in.close();

            } catch (IOException e) {
                throw new Exception("Error al cargar el archivo, archivo no v�lido.");
            }

            int cantidadCriaturas = Integer.parseInt(datosCriaturas.getProperty("criaturas.cantidad"));

            enciclopedia = new Criatura[cantidadCriaturas];
            for (int i = 0; i < cantidadCriaturas; i++) {
                String nombre = datosCriaturas.getProperty("criaturas.criatura" + (i + 1) + ".nombre");
                String gustos = datosCriaturas.getProperty("criaturas.criatura" + (i + 1) + ".gustos");
                String miedos = datosCriaturas.getProperty("criaturas.criatura" + (i + 1) + ".miedos");
                boolean serDeLuz = true;
                int puntos = Integer.parseInt(datosCriaturas.getProperty("criaturas.criatura" + (i + 1) + ".puntos"));

                if (datosCriaturas.getProperty("criaturas.criatura" + (i + 1) + ".serDeLuz").equals("false")) {
                    serDeLuz = false;
                }
                String[] fragmentosRutaImagen = datosCriaturas.getProperty("criaturas.criatura" + (i + 1) + ".ruta")
                        .split("/");
                String rutaImagen = fragmentosRutaImagen[1] + File.separator
                        + fragmentosRutaImagen[2] + File.separator + fragmentosRutaImagen[3];

                enciclopedia[i] = new Criatura(nombre, gustos, miedos, serDeLuz, puntos, rutaImagen);
            }

            criaturaActual = 0;
        } catch (Exception e) {
            throw new Exception("Error al leer el formato del archivo.");
        }
    }

    /**
     * Inicializa el arreglo de criaturas a partir del archivo de configuraci�n. <br>
     * <b>post: </b> Ha sido inicializada la enciclopedia de criaturas.
     *
     * @throws Exception Lanza excepci�n si hay alg�n error al inicializar las criaturas.
     */
    private void inicializarTablero() throws Exception {
        try {
            cantidadMovimientos = Integer.parseInt(datosMapa.getProperty("tablero.cantidadMovimientos"));
            cantidadCriaturas = Integer.parseInt(datosMapa.getProperty("tablero.cantidadCriaturas"));
            int cantidadFilas = Integer.parseInt(datosMapa.getProperty("tablero.cantidadFilas"));
            int cantidadColumnas = Integer.parseInt(datosMapa.getProperty("tablero.cantidadColumnas"));
            tablero = new Casilla[cantidadFilas][cantidadColumnas];
            String[] filaSplit;

            for (int i = 0; i < cantidadFilas; i++) {
                filaSplit = datosMapa.getProperty("tablero.fila" + (i + 1)).split(",");
                for (int j = 0; j < cantidadColumnas; j++) {
                    int tipoCasilla = Integer.parseInt(filaSplit[j]);
                    tablero[i][j] = new Casilla(Casilla.TipoCasilla.valueOf("Tipo" + tipoCasilla), null);
                }
            }

            for (int i = 0; i < cantidadCriaturas; i++) {
                filaSplit = datosMapa.getProperty("tablero.criatura" + (i + 1)).split(",");
                tablero[Integer.parseInt(filaSplit[1])][Integer.parseInt(filaSplit[2])]
                        .setCriatura(buscarCriatura(filaSplit[0]));
            }

        } catch (NumberFormatException e) {
            throw new Exception("Error al leer el formato del archivo.");
        }
    }

    /**
     * Retorna la cantidad de criaturas en la fila especificada. Esta cantidad se genera aleatoriamente.
     *
     * @param pFila Fila que se desea consultar.
     * @return Cantidad de criaturas en la fila especificada.
     */
    public int darCantidadCriaturasPorFila(int pFila) {
        double valorAleatorio = Math.random();
        return (int) (5 * valorAleatorio);
    }

    /**
     * Retorna la cantidad de criaturas en la columna especificada. Esta cantidad se genera aleatoriamente.
     *
     * @param pColumna Columna que se desea consultar.
     * @return Cantidad de criaturas en la columna especificada.
     */
    public int darCantidadCriaturasPorColumna(int pColumna) {
        double valorAleatorio = Math.random();
        return (int) (5 * valorAleatorio);
    }

    /**
     * Retorna el puntaje total que se puede obtener si se encuentran todas las criaturas el cuadrante especificado. Esta cantidad se genera aleatoriamente.
     *
     * @param pCuadrante Cuadrante que se desea consultar. pCuadrante > 0 && pCuadrante <= 4
     * @return El puntaje que se puede obtener en cuadrante especificado.
     */
    public int calcularPuntajePorCuadrante(int pCuadrante) {
        double valorAleatorio = Math.random();
        return (int) (2000 * valorAleatorio);
    }

    /**
     * Retorna la criatura de luz no encontrada que tiene el mayor puntaje. Siempre retorna el drag�n.
     *
     * @return Criatura con el mayor puntaje.
     */
    public Criatura darCriaturaMayorPuntaje() {
        Criatura mayorPuntaje = null;
        for(int i = 0; i < tablero.length; i++)
        {
            for(int j = 0; j < tablero[i].length; j++)
            {
                if(tablero[i][j].getCriatura() != null && !tablero[i][j].isActualizado())
                {
                    if(mayorPuntaje == null)
                    {
                        mayorPuntaje = tablero[i][j].getCriatura();
                    }
                    else if(tablero[i][j].getCriatura().darPuntos() > mayorPuntaje.darPuntos())
                    {
                        mayorPuntaje = tablero[i][j].getCriatura();
                    }
                }
            }
        }
        return mayorPuntaje;
    }

    public Criatura[] getEnciclopedia() {
        return enciclopedia;
    }

    /**
     * Retorna la cantidad de movimientos restantes.
     *
     * @return cantidadMovimientos
     */
    public int getCantidadMovimientos() {
        return cantidadMovimientos;
    }

    /**
     * Retorna la cantidad de criaturas que hay en el tablero.
     *
     * @return cantidadCriaturas
     */
    public int getCantidadCriaturas() {
        return cantidadCriaturas;
    }

    /**
     * Retorna el tablero.
     *
     * @return tablero
     */
    public Casilla[][] getTablero() {
        return tablero;
    }

    // ----------------------------------------------------------------
    // M�todos de Extensi�n
    // ----------------------------------------------------------------

    /**
     * M�todo para la extensi�n 1.
     *
     * @return Respuesta 1.
     */
    public String metodo1() {
        return "Respuesta 1";
    }

    /**
     * M�todo para la extensi�n 2.
     *
     * @return Respuesta 2.
     */
    public String metodo2() {
        return "Respuesta 2";
    }

}
