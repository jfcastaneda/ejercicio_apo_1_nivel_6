package uniandes.cupi2.criaturasMagicas.mundo;

import java.io.File;

public class Casilla {

    /**
     * Enum que representa los diferentes valores que puede tomar una casilla.
     */
    public enum TipoCasilla {
        Tipo0("data" + File.separator + "imagenes" + File.separator + "pradera.png"),
        Tipo1("data" + File.separator + "imagenes" + File.separator + "bosque.png"),
        Tipo2("data" + File.separator + "imagenes" + File.separator + "oceano.png"),
        Tipo3("data" + File.separator + "imagenes" + File.separator + "cueva.png"),
        Tipo4("data" + File.separator + "imagenes" + File.separator + "visitada.png");

        /**
         * Ruta de la imagen de la casilla.
         */
        private String ruta;

        /**
         * Constructor
         *
         * @param ruta
         */
        TipoCasilla(String ruta) {
            this.ruta = ruta;
        }

        /**
         * Retorna ruta de la imagen de la casilla.
         *
         * @return ruta de la imagen
         */
        public String getRuta() {
            return ruta;
        }
    }

    /**
     * Tipo de la casilla.
     */
    private TipoCasilla tipoCasilla;

    /**
     * Criatura presente. Null si no hay ninguna.
     */
    private Criatura criatura;

    /**
     * Ya fue actualizado.
     */
    private boolean actualizado;

    /**
     * Constructor
     *
     * @param tipoCasilla
     * @param criatura
     */
    public Casilla(TipoCasilla tipoCasilla, Criatura criatura) {
        this.tipoCasilla = tipoCasilla;
        this.criatura = criatura;
        this.actualizado = false;
    }

    /**
     * Metodo que gestiona lo que pasa cuando se da clic a la casilla.
     */
    public int click() {
        tipoCasilla = TipoCasilla.Tipo4;
        if (criatura != null) {
            return criatura.darPuntos();
        }
        return 0;
    }

    /**
     * Retorna el tipo de casilla.
     *
     * @return tipoCasilla
     */
    public TipoCasilla getTipoCasilla() {
        return tipoCasilla;
    }

    /**
     * Retorna la criatura de la casilla.
     *
     * @return criatura
     */
    public Criatura getCriatura() {
        return criatura;
    }

    /**
     * Retorna si la casilla ya fue actualizada
     *
     * @return true si ya esta actualizado, false si no
     */
    public boolean isActualizado() {
        return actualizado;
    }

    /**
     * Se actualizo la casilla.
     */
    public void actualizar() {
        actualizado = true;
    }

    /**
     * Configura un nuevo valor de criatura para la casilla.
     *
     * @param criatura
     */
    public void setCriatura(Criatura criatura) {
        this.criatura = criatura;
    }
}
